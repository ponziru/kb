﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogPanel : MonoBehaviour
{
    public Main main;
    public Text caption;
    public Text message;

    // Use this for initialization
    void Start ()
    {
        main = FindObjectOfType<Main>();
        caption.text = main.cardStack[0].caption;
        message.text = main.cardStack[0].message;
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void No()
    {
        main.ExtractNoAnswer();
        main.StartCoroutine("NpcMoveOut");
        Destroy(gameObject);
    }

    public void Yes()
    {
        main.ExtractYesAnswer();
        main.StartCoroutine("NpcMoveOut");
        Destroy(gameObject);
    }
}
