﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardEvent
{
    public int res1, res2, res3, res4, res5, res6, res7, res8;
    public int id;
    public string caption;
    public string message;
    public int messageNumber; //сделать список диалогов если будет больше 1
    public bool isMarkedToDeletionWhenNo;
    public bool isMarkedToDeletionWhenYes;
    public bool isMarkedToDeletion;
    public CardEvent(int eventId, bool yes, bool no)
    {
        isMarkedToDeletionWhenYes = yes;
        isMarkedToDeletionWhenNo = no;
        id = eventId;

        switch (eventId)
        {
            case 0:
                {

                            res1 = 10;
                            res2 = 10;
                            res3 = -10;
                            res4 = -10;

                            res5 = 0;
                            res6 = 0;
                            res7 = 0;
                            res8 = 0;

                    caption = "Hello!";
                    message = "I need help!0\n" + res1 + " " + res2 + " " + res3 + " " + res4 + " \n"
                        + res5 + " " + res6 + " " + res7 + " " + res8;
                    break;
                }
            case 1:
                {
                   
                            res1 = 10;
                            res2 = -10;
                            res3 = 10;
                            res4 = -10;

                            res5 = -10;
                            res6 = 10;
                            res7 = -10;
                            res8 = 10;
                        
                    caption = "Hello!2";
                    message = "I need help!1\n" + res1 + " " + res2 + " " + res3 + " " + res4 + " \n"
                        + res5 + " " + res6 + " " + res7 + " " + res8;
                    break;
                }
            case 2:
                {
                    
                            res1 = 10;
                            res2 = -10;
                            res3 = -10;
                            res4 = 10;

                            res5 = -10;
                            res6 = 10;
                            res7 = 10;
                            res8 = -10;
                       
                    caption = "Hello!3";
                    message = "I need help!2\n" + res1 + " " + res2 + " " + res3 + " " + res4 + " \n"
                        + res5 + " " + res6 + " " + res7 + " " + res8;
                    break;
                }
            case 3:
                {
                   
                            res1 = 10;
                            res2 = 10;
                            res3 = 10;
                            res4 = 10;

                            res5 = -10;
                            res6 = -10;
                            res7 = -10;
                            res8 = -10;
                        
                    caption = "Hello!4";
                    message = "I need help!3\n" + res1 + " " + res2 + " " + res3 + " " + res4 + " \n"
                        + res5 + " " + res6 + " " + res7 + " " + res8;
                    break;
                }
            case 4:
                {
                    
                            res1 = 0;
                            res2 = 0;
                            res3 = 0;
                            res4 = 0;

                            res5 = 0;
                            res6 = 0;
                            res7 = 0;
                            res8 = 0;
                        
                    caption = "Hello!5";
                    message = "I need help!4\n" + res1 + " " + res2 + " " + res3 + " " + res4 + " \n"
                        + res5 + " " + res6 + " " + res7 + " " + res8;
                    break;
                }
            case 5:
                {
                    
                            res1 = -10;
                            res2 = -10;
                            res3 = 10;
                            res4 = 10;

                            res5 = 0;
                            res6 = 0;
                            res7 = 0;
                            res8 = 0;
                       
                    caption = "Hello!6";
                    message = "I need help!5\n" + res1 + " " + res2 + " " + res3 + " " + res4 + " \n"
                        + res5 + " " + res6 + " " + res7 + " " + res8;
                    break;
                }
            case 6:
                {
                    
                            res1 = 10;
                            res2 = 10;
                            res3 = 10;
                            res4 = 10;

                            res5 = 0;
                            res6 = 0;
                            res7 = 0;
                            res8 = 0;
                       
                    caption = "Hello!7";
                    message = "I need help!6\n" + res1 + " " + res2 + " " + res3 + " " + res4 + " " + " \n"
                        + res5 + " " + res6 + " " + res7 + " " + res8;
                    break;
                }
            case 7:
                {
                    
                            res1 = 0;
                            res2 = 0;
                            res3 = 0;
                            res4 = 0;

                            res5 = 0;
                            res6 = 0;
                            res7 = 0;
                            res8 = 0;
                       
                    caption = "Hello!8";
                    message = "I need help!7\n" + res1 + " " + res2 + " " + res3 + " " + res4 + " \n"
                        + res5 + " " + res6 + " " + res7 + " " + res8;
                    break;
                }
            case 8:
                {
                            res1 = -100;
                            res2 = 0;
                            res3 = 0;
                            res4 = 0;

                            res5 = 0;
                            res6 = 0;
                            res7 = 0;
                            res8 = 0;
                      
                    caption = "Hello!8";
                    message = "Privet ja Penek, davaj posadim les na vse tvoi den'gi?\n" + res1 + " " + res2 + " " + res3 + " " + res4 + " \n"
                        + res5 + " " + res6 + " " + res7 + " " + res8;
                    break;
                }
            case 9:
                {
                            res1 = +400;
                            res2 = 0;
                            res3 = 0;
                            res4 = 0;

                            res5 = -20;
                            res6 = 0;
                            res7 = 0;
                            res8 = 0;
                        
                    caption = "Hello!8";
                    message = "Privet ja Drovosek, nam nuzhno derevo dlja prijuta bezdomnyh detej, davaj srubim von tot lesok?\n" + res1 + " " + res2 + " " + res3 + " " + res4 + " \n"
                        + res5 + " " + res6 + " " + res7 + " " + res8;
                    break;
                }
    
        }
    }

        
}
