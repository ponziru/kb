﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DayResultPanel : MonoBehaviour
{
    public Text caption;
    public Text result;
    Main main;
    // Use this for initialization
    void Start ()
    {
        main = FindObjectOfType<Main>();
        result.text = "res1: " + main.res1Store
                      + "\nres2: " + main.res2Store
                      + "\nres3: " + main.res3Store
                      + "\nres4: " + main.res4Store;
        caption.text = "DAY " + main.dayStore + " RESULT";
    }
	
    public void Ok()
    {
        main.res1Store = 0;
        main.res2Store = 0;
        main.res3Store = 0;
        main.res4Store = 0;
        main.turnNumber = 0;

        //main.FillCards();

        //main.Shuffle();
        main.GetNewCard();
        
        Destroy(gameObject); 
    }


}
