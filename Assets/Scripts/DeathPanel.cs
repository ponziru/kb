﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathPanel : MonoBehaviour
{
    Main main;
	// Use this for initialization
	void Start ()
    {
        main = FindObjectOfType<Main>();
	}
	
    public void Ok()
    {
        main.Restart();
        Destroy(gameObject);
    }
}
