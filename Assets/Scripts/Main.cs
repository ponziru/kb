﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;

public class Main : MonoBehaviour
{
    public bool story1;
    public bool story11;

    public Text resText, cardText;
    public int res1, res2, res3, res4;
    public int res1Store, res2Store, res3Store, res4Store, dayStore;
    public List<CardEvent> cardStack = new List<CardEvent>(); //Нужно ли забирать карту из стэка?
    public bool isProcessinginProgress;
    public int turnNumber;
    public GameObject npc;
    public GameObject king;
    public GameObject npcPrefab;
    public GameObject dialogPrefab;
    public GameObject resultPrefab;
    public GameObject deathPrefab;

    public Button yesButton;
    public Button noButton;
    public Text messageText;

    // Use this for initialization
    void Start ()
    {
        
        

        FillCards();
        Shuffle();
        Restart();
        
        
        //GetNewCard();
	}
	
	// Update is called once per frame
	void Update ()
    {
        resText.text = "" + res1 + "  " + res2 + "  " + res3 + "  " + res4;

        if (cardStack.Count > 7)
        {
            cardText.text = "0: " + cardStack[0].id + "\n"
                        + "1: " + cardStack[1].id + "\n"
                        + "2: " + cardStack[2].id + "\n"
                        + "3: " + cardStack[3].id + "\n"
                        + "4: " + cardStack[4].id + "\n"
                        + "5: " + cardStack[5].id + "\n"
                        + "6: " + cardStack[6].id + "\n"
                        + "7: " + cardStack[7].id;
        }
        else if (cardStack.Count > 0)
        {
            cardText.text = "0: " + cardStack[0].id + "\n"
                        + "1: " + cardStack[1].id + "\n"
                        + "2: " + cardStack[2].id + "\n"
                        + "3: " + cardStack[3].id + "\n"
                        + "4: " + cardStack[4].id;
        }
        

        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            StartCoroutine("NpcMoveIn");
        }
        else if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            StartCoroutine("NpcMoveOut");
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            CardEvent temp = cardStack[0];
            cardStack.RemoveAt(0);
            //cardStack.Insert(cardStack.Count, temp);
            cardStack.Insert((int)(cardStack.Count*0.5f), temp);

        }
        
    }

    public void Shuffle()
    {
        for (int i = 0; i < cardStack.Count; i++)
        {
            CardEvent temp = cardStack[i];
            int randomIndex = Random.Range(i, cardStack.Count);
            cardStack[i] = cardStack[randomIndex];
            cardStack[randomIndex] = temp;
        }
    }

    public void FillCards()
    {
        //cardStack.Clear();
        for (int i = 0; i < 8; i++)
        {
            cardStack.Add(new CardEvent(i, false, false));
        }
    }
            

    public void GetNewCard()
    {
        if (res1 < 0 || res2 < 0 || res3 < 0 || res4 < 0)
        {
            king.GetComponent<Animator>().Play("death");
            GameObject go = Instantiate(deathPrefab, GameObject.Find("Canvas").transform);
            return;
        }

        CardEvent ceTemp = new CardEvent(0, false, false);
        bool ceBool = false;
        foreach (CardEvent ce in cardStack)
        {
            if (ce.isMarkedToDeletion)
            {
                ceTemp = ce;
                ceBool = true;
            }
        }
        if (ceBool)
        {
            cardStack.Remove(ceTemp);
        }

        isProcessinginProgress = true;

        CardEvent temp = cardStack[0];
        cardStack.RemoveAt(0);
        cardStack.Insert(Random.Range((int)(cardStack.Count*0.5f), cardStack.Count), temp);

        

        if(story1 && !story11)
        {
            cardStack.Insert(Random.Range(0, 7), new CardEvent(9, true, false));
            story11 = true;
        }
        if (res1 >= 100 && !story1)
        {
            cardStack.Insert(0, new CardEvent(8, true, false));
            story1 = true;
        }
        npc = Instantiate(npcPrefab, new Vector3(Camera.main.ScreenToWorldPoint(new Vector3(Screen.width*1.25f,0,0)).x , - 0.5f), Quaternion.identity);
        float temp1 = Random.value;
        float temp2 = Random.value;
        float temp3 = Random.value;

        npc.GetComponent<SkeletonAnimator>().Skeleton.FindSlot("dagger").R = temp1;
        npc.GetComponent<SkeletonAnimator>().Skeleton.FindSlot("dagger").G = temp2;
        npc.GetComponent<SkeletonAnimator>().Skeleton.FindSlot("dagger").B = temp3;
        
        StartCoroutine("NpcMoveIn");
        
        
    }

    public IEnumerator NpcMoveOut()
    {
        npc.GetComponent<Animator>().Play("run");
        if (npc.transform.localScale.x > 0)
        {
            npc.transform.localScale = new Vector3(-1f, 1f, 1f);
        }
        
        while (Camera.main.WorldToScreenPoint(npc.transform.position).x < (Screen.width * 1.25f)) //((player.transform.position - enemy.transform.position).magnitude < 10)
        {
            npc.transform.position += new Vector3(2f * Time.deltaTime, 0, 0);
            yield return null;
        }
        Destroy(npc);
        if (turnNumber < 8)
        {
            GetNewCard();
        }
        else
        {
            EndOfTheDay();
        }
    }

    public IEnumerator NpcMoveIn()
    {
        npc.GetComponent<Animator>().Play("run");
        if (npc.transform.localScale.x < 0)
        {
            npc.transform.localScale = new Vector3(1f, 1f, 1f);
        }
        while (Camera.main.WorldToScreenPoint(npc.transform.position).x > (Screen.width * 0.85)) //((player.transform.position - enemy.transform.position).magnitude < 10)
        {
            npc.transform.position += new Vector3(-2f * Time.deltaTime, 0, 0);
            yield return null;
        }
        npc.GetComponent<Animator>().Play("idle");
        isProcessinginProgress = false;
        messageText.gameObject.SetActive(true);
        yesButton.gameObject.SetActive(true);
        noButton.gameObject.SetActive(true);
        messageText.text = cardStack[0].message;
        //GameObject go = Instantiate(dialogPrefab, GameObject.Find("Canvas").transform);

    }

    public void EndOfTheDay()
    {
        if (res1 < 0 || res2 < 0 || res3 < 0 || res4 < 0)
        {
            king.GetComponent<Animator>().Play("death");
            GameObject go2 = Instantiate(deathPrefab, GameObject.Find("Canvas").transform);
            return;
        }
        dayStore++;
        GameObject go = Instantiate(resultPrefab, GameObject.Find("Canvas").transform);
    }

    public void ExtractNoAnswer()
    {
        res1 += cardStack[0].res5;
        res2 += cardStack[0].res6;
        res3 += cardStack[0].res7;
        res4 += cardStack[0].res8;
        turnNumber++;
        res1Store += cardStack[0].res5;
        res2Store += cardStack[0].res6;
        res3Store += cardStack[0].res7;
        res4Store += cardStack[0].res8;
        StartCoroutine("NpcMoveOut");
        messageText.gameObject.SetActive(false);
        yesButton.gameObject.SetActive(false);
        noButton.gameObject.SetActive(false);

        if (cardStack[0].isMarkedToDeletionWhenNo)
        {
            cardStack[0].isMarkedToDeletion = true;
        }
    }

    public void ExtractYesAnswer()
    {
        res1 += cardStack[0].res1;
        res2 += cardStack[0].res2;
        res3 += cardStack[0].res3;
        res4 += cardStack[0].res4;
        turnNumber++;
        res1Store += cardStack[0].res1;
        res2Store += cardStack[0].res2;
        res3Store += cardStack[0].res3;
        res4Store += cardStack[0].res4;
        StartCoroutine("NpcMoveOut");
        messageText.gameObject.SetActive(false);
        yesButton.gameObject.SetActive(false);
        noButton.gameObject.SetActive(false);

        if (cardStack[0].isMarkedToDeletionWhenYes)
        {
            cardStack[0].isMarkedToDeletion = true;
        }
    }

    public void Restart()
    {
        king.GetComponent<Animator>().Play("idle");
        res1 = 50;
        res2 = 50;
        res3 = 50;
        res4 = 50;
        res1Store = 0;
        res2Store = 0;
        res3Store = 0;
        res4Store = 0;
        turnNumber = 0;
        dayStore = 0;

        if (npc != null)
        {
            Destroy(npc);
        }
        cardStack.Clear();
        FillCards();
        Shuffle();
        GetNewCard();

    }

    
}
